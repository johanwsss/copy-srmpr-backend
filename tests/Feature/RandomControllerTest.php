<?php

namespace Tests\Unit;

use Tests\TestCase;

class RandomControllerTest extends TestCase
{
    /**
     * Test if random animal is in the allowed values.
     *
     * @return void
     */
    public function test_random_animal()
    {
        $response = $this->get('/api/animal');
        $response->assertStatus(200);
        $content = json_decode($response->getContent(), true);
        $this->assertContains($content['data']['value'], config('constants.animals'));
    }

    /**
     * Test if random city is in the allowed values.
     *
     * @return void
     */
    public function test_random_city()
    {
        $response = $this->get('/api/city');
        $response->assertStatus(200);
        $content = json_decode($response->getContent(), true);
        $cities = config('constants.cities');
        $this->assertContains($content['data']['city'], $cities);
        $this->assertContains($content['data']['country'], array_keys($cities));
    }

    /**
     * Test if random number is in the allowed values.
     *
     * @return void
     */
    public function test_random_number()
    {
        $response = $this->get('/api/number');
        $response->assertStatus(200);
        $content = json_decode($response->getContent(), true);
        $this->assertContains($content['data']['value'], range(1, 100));
    }
}
